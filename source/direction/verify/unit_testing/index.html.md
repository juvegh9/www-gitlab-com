---
layout: markdown_page
title: "Category Vision - Unit Testing"
---

- TOC
{:toc}

## Unit Testing

Unit testing ensures that individual components built within a pipeline perform as expected, and are an important part of a Continuous Integration framework.

- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3A%3AUnit%20Testing)
- [Overall Vision](/direction/verify)
- [UX Research](https://gitlab.com/groups/gitlab-org/-/epics/592)

Interested in joining the conversation for this category? Please join us in our
[public epic](https://gitlab.com/groups/gitlab-org/-/epics/TBD) where
we discuss this topic and can answer any questions you may have. Your contributions
are more than welcome.

## What's Next & Why

TBD

## Maturity Plan

TBD

## Competitive Landscape

TBD

## Top Customer Success/Sales Issue(s)

TBD

## Top Customer Issue(s)

TBD

## Top Internal Customer Issue(s)

TBD

## Top Vision Item(s)

TBD

