---
layout: markdown_page
title: "Merchandise handling"
---

## On this page
{:.no_toc}

- TOC
{:toc}

----

## Daily routine

One of the main community advocate task is to fulfill all the swag orders.

### Swag orders

Advocates should check for the new orders regularly as the orders should be fulfilled and shipped in a timely manner. In order to process these orders you'll have to fulfill them both in Shopify and Printfection.

#### Printfection

Fulfilling in Printfection means that Prinfection will pack and ship the items.

1. Login to Printfection
2. Go to the "Shopify store orders" collection under the campaign tab.
3. Go to the manage tab
4. Make sure that all the orders from Shopify are shown in there and that everything is okay.
5. Press the "place the order button".

That's all, Printfection will handle the rest. 

Note: Printfection also sends the email confirmation to the customer.

#### Shopify

Fulfilling in Shopify serves to send the actual notification that an order has been processed, including the invoice for the customer.

1. Login to Shopify
2. Go to the orders page
3. Make sure to check all the new orders highlighted with the yellow Unfulfilled tag.
4. Click on actions > fulfill selected orders
5. Make sure that the default "send a notification" option is selected and press the fulfill button.

That's all, the customers should receive their confirmations automatically.


